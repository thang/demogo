// main.go
package main

import (
	"connect/dbconect"
	"connect/redisconnect"
	"database/sql"
	"fmt"
	"strconv"

	_ "github.com/go-sql-driver/mysql"
	// "github.com/go-redis/redis"
	// "config"
)

func setCustomers(offset int, length int, chnl chan int) {

	fmt.Println("=====================")
	fmt.Println("start data %v", offset)
	fmt.Println("=====================")

	client := redisconnect.CreateCon(1)
	client.FlushDB()

	// Open database connection
	db := dbconect.CreateCon()
	defer db.Close()

	// Execute the query
	rows, err := db.Query("SELECT * FROM st_customers limit " + strconv.Itoa(offset) + "," + strconv.Itoa(length))
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	// Get column names
	columns, err := rows.Columns()
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	// Make a slice for the values
	values := make([]sql.RawBytes, len(columns))

	// rows.Scan wants '[]interface{}' as an argument, so we must copy the
	// references into such a slice
	// See http://code.google.com/p/go-wiki/wiki/InterfaceSlice for details
	scanArgs := make([]interface{}, len(values))
	for i := range values {
		scanArgs[i] = &values[i]
	}

	// Fetch rows
	for rows.Next() {
		// get RawBytes from data
		err = rows.Scan(scanArgs...)
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}

		// Now do something with the data.
		// Here we just print each column as a string.
		var value string
		var id string
		MyMap := make(map[string]interface{})
		for i, col := range values {
			// Here we can check if the value is nil (NULL value)
			if col == nil {
				value = "NULL"
			} else {
				value = string(col)
			}
			if columns[i] == "id" {
				id = value
			}

			// fmt.Println(columns[i], ": ", value)
			if value != "NULL" {
				MyMap[columns[i]] = value
			}
		}

		client.HMSet("db:customers:"+id, MyMap).Result()
	}
	if err = rows.Err(); err != nil {
		panic(err.Error()) //
	}

	fmt.Println("=====================")
	fmt.Println("received data")
	fmt.Println("=====================")
	chnl <- offset

}

func main() {

	ch := make(chan int)

	sum := 0
	for sum < 100000 {
		go setCustomers(sum, 10000, ch)
		sum += 10000
	}

	sumGo := 0
	for sumGo < 100000 {
		fmt.Println("Received ch", <-ch)
		sumGo += 10000
	}

	fmt.Println("Main received data")

}
