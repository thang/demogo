package config

import (
	"encoding/json"
	"io/ioutil"
)

type Config struct {
	MySQL struct {
		Host     string `json:"host"`
		User     string `json:"user"`
		Password string `json:"password"`
		DB       string `json:"db"`
	} `json:"mysql"`
	REDIS struct {
		Host     string `json:"host"`
		Password string `json:"password"`
		DB       int `json:"db"`
	} `json:"redis"`
}


func FromFile(path string) (*Config, error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	var cfg Config
	if err := json.Unmarshal(b, &cfg); err != nil {
		return nil, err
	}

	return &cfg, nil
}
