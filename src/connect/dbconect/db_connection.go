// db_conection.go
package dbconect

import (
	"config"
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

const defaultConfigPath = "./src/config/config.json"

func CreateCon() *sql.DB {

	config, err := config.FromFile(defaultConfigPath)

	if err != nil {
		panic(err)
	}

	db, err := sql.Open("mysql", config.MySQL.User+":"+config.MySQL.Password+"@tcp("+config.MySQL.Host+")/"+config.MySQL.DB)
	if err != nil {
		fmt.Println(err.Error())
	} else {
		// fmt.Println("db is connected")
	}
	//defer db.Close()
	// make sure connection is available
	err = db.Ping()

	if err != nil {
		fmt.Println("MySQL db is not connected")
		fmt.Println(err.Error())
	}
	fmt.Println("Connect db success")
	return db
}
