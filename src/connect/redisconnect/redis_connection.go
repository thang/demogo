// redis_conection.go
package redisconnect

import (
	"config"
	"fmt"
	"reflect"

	"github.com/go-redis/redis"
)

const defaultConfigPath = "./src/config/config.json"

func CreateCon(db int) *redis.Client {

	config, err := config.FromFile(defaultConfigPath)

	if err != nil {
		panic(err)
	}

	if db == -1 {
		db = config.REDIS.DB
	}
	client := redis.NewClient(&redis.Options{
		Addr:     config.REDIS.Host,
		Password: config.REDIS.Password, // no password set
		DB:       db,                    // use default DB
	})

	pong, err := client.Ping().Result()
	fmt.Println(pong, err)
	return client
}

func ToMap(in interface{}, tag string) (map[string]interface{}, error) {
	out := make(map[string]interface{})

	v := reflect.ValueOf(in)
	if v.Kind() == reflect.Ptr {
		v = v.Elem()
	}

	// we only accept structs
	if v.Kind() != reflect.Struct {
		return nil, fmt.Errorf("ToMap only accepts structs; got %T", v)
	}

	typ := v.Type()
	for i := 0; i < v.NumField(); i++ {
		// gets us a StructField
		fi := typ.Field(i)
		if tagv := fi.Tag.Get(tag); tagv != "" {
			if value := v.Field(i).Interface(); value != "" {
				// set key of map to value in struct field
				out[tagv] = value
			}
		}
	}
	return out, nil
}

func demo() {

	// client := redisconnect.CreateCon()

	// pong, err := client.Ping().Result()
	// fmt.Println(pong, err)
	// fmt.Println("%v", model.ColArticle)

	// //zset

	// err = client.ZAdd("zset", &redis.Z{Score: 2, Member: "one"}).Err()
	// err = client.ZAdd("zset", &redis.Z{Score: 3, Member: "two"}).Err()
	// err = client.ZAdd("zset", &redis.Z{Score: 1, Member: "three"}).Err()
	// vals, err := client.ZRangeWithScores("zset", 0, -1).Result()
	// fmt.Println("%v", vals)

	// // set
	// err = client.Set("key", "value", 0).Err()
	// if err != nil {
	// 	panic(err)
	// }
	// val, err := client.Get("key").Result()
	// if err != nil {
	// 	panic(err)
	// }
	// fmt.Println("key", val)

	// // hash

	// ok, err := client.HMSet("hash", map[string]interface{}{
	// 	"key1": "hello1",
	// 	"key2": "hello2",
	// }).Result()
	// fmt.Println("%v", ok)

	// v, err := client.HGetAll("hash").Result()
	// fmt.Println("%v", v)
}
